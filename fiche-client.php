<?php
session_start();
if (!$_SESSION["connected_user"]) {
    header("Location: login.php");
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fiches client</title>
</head>
<body>
<header>
    <h2>Fiches client</h2>
</header>

<section>
    <article>
        <div class="liste">
            <table>
                <tr>
                    <th>Nom et prénom</th>
                    <th>Solde</th>
                    <th>Profil</th>
                    <th>Virement</th>
                </tr>
                <?php
                foreach ($_SESSION['listeUsers'] as $user) {
                    echo '<tr>';
                    echo '<td>' . $user['nom'] . ' ' . $user['prenom'] . '</td>';
                    echo '<td>' . $user['solde_compte'] . '</td>';
                    echo '<td>' . $user['profil_user'] . '</td>';
                    echo '<td><a href="virement.php?numero_compte=' . $user['numero_compte'] . '"/>Faire un virement</td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>

    </article>
</section>
</body>
</html>