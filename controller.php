<?php
require_once('model.php');

session_start();

// URL de redirection par défaut (si pas d'action ou action non reconnue)
$url_redirect = "index.php";

if (isset($_REQUEST['action'])) {

    if (htmlspecialchars($_REQUEST['action']) == 'authenticate') {
        /* ======== AUTHENT ======== */
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || htmlspecialchars($_REQUEST['login']) == "" || htmlspecialchars($_REQUEST['mdp']) == "") {
            // manque login ou mot de passe
            $url_redirect = "login.php?nullvalue";

        } else {

            $utilisateur = findUserByLoginPwd(htmlspecialchars($_REQUEST['login']), htmlspecialchars($_REQUEST['mdp']));

            if ($utilisateur == false) {
                // echec authentification
                $url_redirect = "login.php?badvalue";

            } else {
                echo $utilisateur;
                // authentification réussie
                $_SESSION["connected_user"] = $utilisateur;
                $_SESSION["listeUsers"] = findAllUsers();
                $_SESSION["employedUsers"] = findEmployedUsers();
                $url_redirect = "accueil.php";
            }
        }

    } else if (htmlspecialchars($_REQUEST['action']) == 'disconnect') {
        /* ======== DISCONNECT ======== */
        session_destroy();
        $url_redirect = "login.php";

    } else if (htmlspecialchars($_REQUEST['action']) == 'transfert') {
        if (isset($_SESSION['token']) AND isset($_REQUEST['token']) AND !empty($_SESSION['token']) AND !empty($_REQUEST['token'])) {
            if ($_SESSION['token'] == $_REQUEST['token']) {

                /* ======== TRANSFERT ======== */
                if ($_SESSION['connected_user']['numero_compte'] !== htmlspecialchars($_REQUEST['destination'])) {

                    if (is_numeric(htmlspecialchars($_REQUEST['montant']))) {
                        transfert(htmlspecialchars($_REQUEST['destination']), $_SESSION["connected_user"]["numero_compte"], htmlspecialchars($_REQUEST['montant']));
                        $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] - htmlspecialchars($_REQUEST['montant']);
                        $url_redirect = "virement.php?trf_ok";

                    } else {
                        $url_redirect = "virement.php?bad_mt=" . htmlspecialchars($_REQUEST['montant']);
                    }
                } else {
                    $url_redirect = "virement.php?bad_destination";
                }
            }
        } else {
            echo 'Erreur de token, pirate !';
        }

    } else if (htmlspecialchars($_REQUEST['action']) == 'sendmsg') {
        /* ======== MESSAGE ======== */
        addMessage(htmlspecialchars($_REQUEST['to']), $_SESSION["connected_user"]["id_user"], htmlspecialchars($_REQUEST['sujet']), htmlspecialchars($_REQUEST['corps']));
        $url_redirect = "accueil.php?msg_ok";

    } else if (htmlspecialchars($_REQUEST['action']) == 'msglist') {
        /* ======== MESSAGE ======== */
        $_SESSION['messagesRecus'] = findMessagesInbox($_SESSION["connected_user"]["id_user"]);
        $url_redirect = "messagerie.php";

    }


}

header("Location: $url_redirect");

?>
