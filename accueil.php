<?php
session_start();
if (!$_SESSION["connected_user"]) {
    header("Location: login.php");
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Mon Compte</title>
    <link rel="stylesheet" type="text/css" media="all" href="css/mystyle.css"/>
</head>
<body>
<header>
    <form method="POST" action="controller.php">
        <input type="hidden" name="action" value="disconnect">
        <button class="btn-logout form-btn">Déconnexion</button>
    </form>

    <h2><?php echo $_SESSION["connected_user"]["prenom"]; ?> <?php echo $_SESSION["connected_user"]["nom"]; ?> - Mon
        compte</h2>
</header>

<section>

    <article>
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Vos informations personnelles</span>
            </div>
            <div class="field">
                <label>Login : </label><span><?php echo $_SESSION["connected_user"]["login"]; ?></span>
            </div>
            <div class="field">
                <label>Profil : </label><span><?php echo $_SESSION["connected_user"]["profil_user"]; ?></span>
            </div>
        </div>
    </article>

    <?php
    if ($_SESSION['connected_user']['profil_user'] == 'EMPLOYE') {
        echo '
                    <article>
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Fiches client</span>
              </div>
              <a href="fiche-client.php">Accéder aux fiches clients</a>
          </div>
        </article>';
    }
    ?>

    <article>
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Compte</span>
            </div>
            <div class="field">
                <label>N° compte : </label><span><?php echo $_SESSION["connected_user"]["numero_compte"]; ?></span>
            </div>
            <div class="field">
                <label>Solde : </label><span><?php echo $_SESSION["connected_user"]["solde_compte"]; ?> &euro;</span>
            </div>
        </div>
    </article>

    <article>
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Virement</span>
            </div>
            <a href="virement.php"> Faire un virement</a>
        </div>
    </article>

    <article>
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Messagerie</span>
            </div>
            <p><a href="controller.php?action=msglist"
                  target="_blank">Accéder à messagerie</a></p>
        </div>
    </article>

</section>

</body>
</html>
