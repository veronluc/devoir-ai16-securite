<?php
session_start();
if (!$_SESSION["connected_user"]) {
    header("Location: login.php");
}
$token = uniqid();

//Protection contre la faille csrf
$_SESSION['token'] = $token;
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Virement</title>
</head>
<body>
<header>
    <h2>
        Faire un virement
    </h2>
</header>
<form method="POST" id="transfertForm" action="controller.php">
    <input type="hidden" name="action" value="transfert">
    <div class="fieldset">
        <div class="fieldset_label">
            <span>Transférer de l'argent</span>
        </div>
        <div class="field">
            <label>N° compte destinataire : </label>

            <?php
            if (!isset($_REQUEST['numero_compte'])) {
                echo '<input type="text" size="20" name="destination">';
            } else {
                echo '<input type="text" size="20" name="destination" value="' . htmlspecialchars($_REQUEST['numero_compte']) . '">';
            }
            ?>
        </div>
        <input type="hidden" name="token" value="<?php echo $token?>">
        <div class="field">
            <label>Montant à transférer : </label><input type="text" size="10" name="montant">
        </div>
        <button class="form-btn" onclick="confirmTransfert()">Transférer</button>
        <?php
        if (isset($_REQUEST["trf_ok"])) {
            echo '<p>Virement effectué avec succès.</p>';
        }
        if (isset($_REQUEST["bad_mt"])) {
            echo '<p>Le montant saisi est incorrect : ' . $_REQUEST["bad_mt"] . '</p>';
        }
        if (isset($_REQUEST["bad_destination"])) {
            echo '<p>Vous ne pouvez pas vous faire des virements à vous-meme !</p>';
        }
        ?>
    </div>
</form>

</body>
</html>

<!--Affichage d'un message de confirmation pour se protéger de la faille CSRF (en plus d'un token de vérification)-->
<script>
    function confirmTransfert() {
        if(confirm('Voulez-vous vraiment faire ce virement ?')) {
            document.getElementById('transfertForm').submit();
        }
    }
</script>