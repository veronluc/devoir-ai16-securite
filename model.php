<?php

function getMySqliConnection()
{
    $db_connection_array = parse_ini_file("config/config.ini");
    return new mysqli($db_connection_array['DB_HOST'], $db_connection_array['DB_USER'], $db_connection_array['DB_PASSWD'], $db_connection_array['DB_NAME']);
}

function findUserByLoginPwd($login, $pwd)
{
    $mysqli = getMySqliConnection();

    // On récupère l'IP du visiteur
    $ip = $_SERVER['REMOTE_ADDR'];

// On regarde s'il est autorisé à se connecter
    $stmt = $mysqli->prepare('SELECT * FROM connexion WHERE ip = ?');
    $stmt->bind_param('s', $ip);
    $stmt->execute();
    $result = $stmt->get_result();
    $count = $result->num_rows;

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
        $utilisateur = false;
    } else {
        if ($count < 5) {
//        Préparation de la requete pour éviter l'injection SQL
            $stmt = $mysqli->prepare("select nom,prenom,login,id_user,numero_compte,profil_user,solde_compte from users where login = ? and mot_de_passe = ?");
            $stmt->bind_param('ss', $login, $pwd);
            $isSuccess = $stmt->execute();
            $result = $stmt->get_result();
            $utilisateur = $result->fetch_assoc();
            if (!$utilisateur) {
                $stmt = $mysqli->prepare('INSERT INTO connexion(ip) VALUES(?)');
                $stmt->bind_param('s', $ip);
                $stmt->execute();
            }
            if (!$isSuccess) {
                echo 'Erreur requête BDD';
                $utilisateur = false;
            }
            $result->free();

            $stmt->close();
            $mysqli->close();
        } else {
            echo 'Banni !';
        }
    }

    return $utilisateur;
}


function findAllUsers()
{
    $mysqli = getMySqliConnection();

    $listeUsers = array();

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    } else {
        $req = "select nom,prenom,login,id_user,solde_compte,numero_compte,profil_user from users";
        if (!$result = $mysqli->query($req)) {
            echo 'Erreur requête BDD [' . $req . '] (' . $mysqli->errno . ') ' . $mysqli->error;
        } else {
            while ($unUser = $result->fetch_assoc()) {
                $listeUsers[$unUser['id_user']] = $unUser;
            }
            $result->free();
        }
        $mysqli->close();
    }

    return $listeUsers;
}

function findEmployedUsers()
{
    $mysqli = getMySqliConnection();

    $listeUsers = array();

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    } else {
        $req = "select nom,prenom,login,id_user from users where profil_user = 'EMPLOYE'";
        if (!$result = $mysqli->query($req)) {
            echo 'Erreur requête BDD [' . $req . '] (' . $mysqli->errno . ') ' . $mysqli->error;
        } else {
            while ($unUser = $result->fetch_assoc()) {
                $listeUsers[$unUser['id_user']] = $unUser;
            }
            $result->free();
        }
        $mysqli->close();
    }

    return $listeUsers;
}


function transfert($dest, $src, $mt)
{
    $mysqli = getMySqliConnection();

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    } else {
//        Requetes préparées pour éviter l'injection sql
        $stmt = $mysqli->prepare("update users set solde_compte=solde_compte+? where numero_compte = ?");
        $stmt->bind_param('is', $mt, $dest);
        $isSuccess = $stmt->execute();
        if (!$isSuccess) {
            echo 'Erreur requête BDD';
        }

        $stmt = $mysqli->prepare("update users set solde_compte=solde_compte-? where numero_compte=?");
        $stmt->bind_param('is', $mt, $src);
        $isSuccess = $stmt->execute();
        if (!$isSuccess) {
            echo 'Erreur requête BDD';
        }

        $stmt->close();
        $mysqli->close();
    }

}


function findMessagesInbox($userid)
{
    $mysqli = getMySqliConnection();

    $listeMessages = array();

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    } else {
        //        Préparation de la requete pour éviter l'injection SQL
        $stmt = $mysqli->prepare("select id_msg,sujet_msg,corps_msg,u.nom,u.prenom from messages m, users u where m.id_user_from=u.id_user and id_user_to=?");
        $stmt->bind_param('s', $userid);
        $isSuccess = $stmt->execute();
        $result = $stmt->get_result();
        while ($unMessage = $result->fetch_assoc()) {
            $listeMessages[$unMessage['id_msg']] = $unMessage;
        }
        $result->free();

        if (!$isSuccess) {
            echo 'Erreur requête BDD';
        }

        $stmt->close();
        $mysqli->close();


    }

    return $listeMessages;
}


function addMessage($to, $from, $subject, $body)
{
    $mysqli = getMySqliConnection();

    if ($mysqli->connect_error) {
        echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
    } else {
        $stmt = $mysqli->prepare("insert into messages(id_user_to,id_user_from,sujet_msg,corps_msg) values(?,?,?,?)");
        $stmt->bind_param('iiss', $to, $from, $subject, $body);
        $isSuccess = $stmt->execute();
        if (!$isSuccess) {
            echo 'Erreur requête BDD';
        }

        $stmt->close();
        $mysqli->close();
    }

}

?>
