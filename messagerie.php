<?php
session_start();
if (!$_SESSION["connected_user"]) {
    header("Location: login.php");
}
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messages</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body>
    <header>
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Messages reçus</h2>
    </header>

    <section>
        <article>
        
          <div class="liste">
            <table>
              <tr><th>Expéditeur</th><th>Sujet</th><th>Message</th></tr>
              <?php
              foreach ($_SESSION['messagesRecus'] as $cle => $message) {
                echo '<tr>';
                echo '<td>'.$message['nom'].' '.$message['prenom'].'</td>';
                echo '<td>'.$message['sujet_msg'].'</td>';
                echo '<td>'.$message['corps_msg'].'</td>';
                echo '</tr>';
              }
               ?>
            </table>
          </div>
    
        </article>
    </section>

    <form method="POST" action="controller.php">
        <input type="hidden" name="action" value="sendmsg">
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Messagerie</span>
            </div>
            <div class="field">
                <label>Destinataire : </label>
                <select name="to">
                    <?php
                    if ($_SESSION['connected_user']['profil_user'] == 'EMPLOYE') {
                        foreach ($_SESSION['listeUsers'] as $id => $user) {
                            echo '<option value="'.$id.'">'.$user['nom'].' '.$user['prenom'].'</option>';
                        }
                    } else {
                        foreach ($_SESSION['employedUsers'] as $id => $user) {
                            echo '<option value="'.$id.'">'.$user['nom'].' '.$user['prenom'].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="field">
                <label>Sujet : </label><input type="text" size="20" name="sujet">
            </div>
            <div class="field">
                <label>Message : </label><textarea name="corps" cols="25" rows="3""></textarea>
            </div>
            <button class="form-btn">Envoyer</button>
            <?php
            if (isset($_REQUEST["msg_ok"])) {
                echo '<p>Message envoyé avec succès.</p>';
            }
            ?>
        </div>
    </form>
</body>
</html>
